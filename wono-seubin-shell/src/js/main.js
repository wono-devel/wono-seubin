var tung;

if (seubinver.spec >= "1.3") {
    console.log("WELLNESS: Right Seubin version")
} else {
    document.querySelector("body").innerHTML = "<p>Book failed to start, check the JavaScript Console for more details</p> <br/> <p>BAD_SEUBIN_SPEC</p>";
    alert("Wrong Specification, Expecting 1.3");
    console.log("MISTAKE: Book is expecting 1.3, but a different version is reported");
    stopAllExecution(); // Crash shell if running on seubin 1.2 or 1.3
}

d.getElementById('rellodhimself').src = sbnfs + sbnfs_setupPath + "/seubin.sbfs/meting/favicon.png"
d.getElementById('unrellodhimself').src = sbnfs + sbnfs_setupPath + "/seubin.sbfs/meting/favicon.png"
d.getElementById('rellodMenuImage').src = sbnfs + sbnfs_setupPath + "/seubin.sbfs/meting/seubin.png"

async function getTungJSON() {  // Get tung JSON
    try {
        const tjson = await fetch("./tungs/" + sbn.tungPref() + ".json");
        return await tjson.json();
    } catch (error) {
        const tjson = await fetch("./tungs/en.json"); //Download English json if the language's json cannot be downloaded
        return await tjson.json();
    }
}

async function polyglotNow() { // javascript devs be like
        var tung = await getTungJSON();
        d.getElementById('RunSmButt').innerHTML = tung.runSidebarText;
        d.getElementById('SettSmButt').innerHTML = tung.settingsSidebarText;
        d.getElementById('HomeSmButt').innerHTML = tung.mainpageSidebarText;
        d.getElementById('AbtSmButt').innerHTML = tung.aboutSidebarText;
        d.getElementById('RecSmButt').innerHTML = tung.recoverySidebarText;
        d.getElementById('runButton').innerHTML = tung.runRunboxText;
        d.getElementById('settingsHeader').innerHTML = tung.settingsAppletHeader;
        d.getElementById('rawputOptHeader').innerHTML = tung.rawputOptHeader;
        d.getElementById('themeHeader').innerHTML = tung.themeHeader;
        d.getElementById('themeBlack').innerHTML = tung.themeBlack;
        d.getElementById('themeClassic').innerHTML = tung.themeClassic;
        d.getElementById('themeWhite').innerHTML = tung.themeWhite;
        d.getElementById('themeAero').innerHTML = tung.themeAero;
        d.getElementById('iconAspHeader').innerHTML = tung.iconRatioHeader;
        d.getElementById('icon96').innerHTML = tung.icon96;
        d.getElementById('icon192').innerHTML = tung.icon192;
        d.getElementById('icon290').innerHTML = tung.icon290;
        d.getElementById('genderHeader').innerHTML = tung.genderHeader;
        d.getElementById('anonKire').innerHTML = tung.genderAnon;
        d.getElementById('wereKire').innerHTML = tung.genderWere;
        d.getElementById('wifeKire').innerHTML = tung.genderWife;
        d.getElementById('boNaHeader').innerHTML = tung.bookNames;
        d.getElementById('bNyes').innerHTML = tung.yes;
        d.getElementById('bNno').innerHTML = tung.no;
        d.getElementById('saveSettings').innerHTML = tung.saveSettings;
        d.getElementById('wipeAllButt').innerHTML = tung.rawputWipeAll;
        d.getElementById('wipedAll').innerHTML = tung.wipedAll;
}

if (!localStorage.getItem('seubinSetupEnded')) {
    console.log("Delaying translation (LsKey seubinSetupEnded doesn't exist!)");
} else {
    polyglotNow();
}

if (!localStorage.getItem('seubinThemePref')) {
    document.getElementById("theme").href="css/black.css";
} else {
    document.getElementById("theme").href="css/" + sbn.themePref() + ".css";
}

if (!localStorage.getItem('wono.shell.bookNamesHidden')) {
    localStorage.setItem('wono.shell.bookNamesHidden', '0');
}

var appletstate = "home";

function runBook() {
    document.getElementById("runMenuDiv").style.display="none";
    booksbfs = document.getElementById("runField").value;
    if (booksbfs == "meow!") { //Easter Egg
        window.location.href = "booklets/sonycat.sbfs";
    } else {
        sbn.launch(booksbfs);
    }
}

function onClickofRellod() { //Open Rellod Menu, Hide Rellod Button and Run box
    document.getElementById("rellodmenu").style.display="block";
    document.getElementById("rellod").style.display="none";
    document.getElementById("runMenuDiv").style.display="none";
}

function onClickofRellod0() { //Show Rellod Button, Hide Rellod Menu
    document.getElementById("rellod").style.display="block";
    document.getElementById("rellodmenu").style.display="none";
}

function runDialog(e) {
    document.getElementById("runMenuDiv").style.display="block";
    document.getElementById("rellodmenu").style.display="none";
    document.getElementById("rellod").style.display="block";
}

function wipeData() {
    katze.rmLs();
    katze.rmCkys();
    alert("Data has been wiped!");
}

function appletSwitch() { //If we are on the home menu, switch to settings menu. Otherwise, if on setting menu, switch to home menu
    if (appletstate === "home") {
        document.getElementById("settings").style.display = "block";
        document.getElementById("booklist").style.display = "none";
        document.getElementById("SettSmButt").style.display = "none";
        document.getElementById("HomeSmButt").style.display = "block";
        appletstate = "settings";
        onClickofRellod0();
    } else if (appletstate === "settings") {
        document.getElementById("booklist").style.display = "flex";
        document.getElementById("settings").style.display = "none";
        document.getElementById("HomeSmButt").style.display = "none";
        document.getElementById("SettSmButt").style.display = "block";
        appletstate = "home";
        onClickofRellod0();
    }
}

function reloadThemes() {
    document.getElementById('theme').href="css/" + sbn.themePref() + ".css";
}

function saveSettings() {
    sbn.setThemePref(document.getElementById('seubinThemeChooser').value);
    sbn.setIconPref(document.getElementById('seubinIconChooser').value);
    sbn.setGender(document.getElementById('seubinGenderChooser').value);
    localStorage.setItem('wono.shell.bookNamesHidden', document.getElementById('seubinBoNaChooser').value);
    reloadThemes();
}

function wipeAllRawput() {
    k.rmLs();
    k.rmCkys();
    alert(d.getElementById("wipedAll").innerHTML);
}

function setTheTung() {
    sbn.setTungPref(document.getElementById("tungFieldSetting").value);
}

d.getElementById("tungFieldSetting").value = ""; //Clear value
d.getElementById("tungFieldSetting").value = sbn.tungPref(); //Show our current speech
d.getElementById("seubinThemeChooser").value = sbn.themePref(); //Show our current theme preference in the theme chooser box
d.getElementById("seubinIconChooser").value = sbn.iconPref(); //Show our current icon preference in the icon ratio box
d.getElementById("seubinGenderChooser").value = sbn.gender(); //Show what we sooging are in the pronoun box lol.
d.getElementById("seubinBoNaChooser").value = localStorage.getItem('wono.shell.bookNamesHidden'); //Hide name pref
