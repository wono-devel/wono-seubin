var iconpng = "/meting/" + sbn.iconPref() + ".png";

async function getBooksJSON() {
  const response = await fetch(sbnfs + sbnfs_setupPath + '/settings.aso/books.json');
  return response.json();
}

async function addBookIcons() {
  let bookString = "";
  const booklsrp = await getBooksJSON();

  booklsrp.forEach(function(book){
    book.url = book.url || book.path;
    if (localStorage.getItem('wono.shell.bookNamesHidden') == 1) {
      bookString += "<a class="+ "book" +" onclick=" + "sbn.launch('" + book.url + "');" + "><img class="+ "bookIcon" +" src=" + book.path + iconpng +"></a>";
    } else {
      bookString += "<a class="+ "book" +" onclick=" + "sbn.launch('" + book.url + "');" + "><img class="+ "bookIcon" +" src=" + book.path + iconpng +"><p class='bookname'>" + book.name + "</p></a>";
    }
  });

  document.getElementById('apps').innerHTML = bookString;
}

if (!localStorage.getItem('seubinSetupEnded')) {
    console.log("The shell is going to not load book icons (LsKey seubinSetupEnded doesn't exist!)");
} else {
    addBookIcons();
    document.getElementById("icon-ratio").href="css/" + sbn.iconPref() + ".css";
    document.getElementById('loadingtext').style.display="none";
}
