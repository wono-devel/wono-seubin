async function getTungJSONForSetup() {  // Get tung JSON
    try {
        const tjson = await fetch("./tungs/" + sbn.tungPref() + ".json");
        return await tjson.json();
    } catch (error) {
        const tjson = await fetch("./tungs/en.json"); //Download English json if the language's json cannot be downloaded
        return await tjson.json();
    }
}

async function polyglotNowForSetup() { // main function for localization and crap
        const tungSetup = await getTungJSON();
        d.getElementById('setupThemeP').innerHTML = tungSetup.setupTheme;
        d.getElementById('themeBlackSetup').innerHTML = tungSetup.themeBlack; d.getElementById('themeWhiteSetup').innerHTML = tungSetup.themeWhite; d.getElementById('themeClassicSetup').innerHTML = tungSetup.themeClassic; d.getElementById('themeAeroSetup').innerHTML = tungSetup.themeAero;
        d.getElementById('setupIconP').innerHTML = tungSetup.setupIcon;
        d.getElementById('icon96Setup').innerHTML = tungSetup.icon96; d.getElementById('icon192Setup').innerHTML = tungSetup.icon192; d.getElementById('icon290Setup').innerHTML = tungSetup.icon290;
        d.getElementById('setupFinishP').innerHTML = tungSetup.setupFinish;
        d.getElementById('setup-theme-pref').innerHTML = tungSetup.setupFinishTheme;
        d.getElementById('setup-icon-pref-prev').innerHTML = tungSetup.setupFinishIcon;
        d.getElementById('nowRebootMsgSetup').innerHTML = tungSetup.rebootNowMsgSetup;
        d.getElementById('disabledRellod').innerHTML = tungSetup.rellodDisabled;
}

if (!localStorage.getItem('seubinSetupEnded')) {
    console.log("Starting Setup Screen (LsKey seubinSetupEnded doesn't exist!)");
    startSetup();
}

function typeTungPref() { //Work around for tung field
    nextStepTung(document.getElementById('tungField').value);
}

function nextStepTung(tung) {
    sbn.setTungPref(tung);
    document.getElementById("setup-tung").style.display="none";
    polyglotNowForSetup();
    document.getElementById("setup-theme").style.display="block";
}

function nextStepTheme(theme) {
    sbn.setThemePref(theme);
    document.getElementById("setup-theme").style.display="none";
    document.getElementById("setup-iconsize").style.display="block";
    document.getElementById("theme").href="css/" + sbn.themePref() + ".css";
}

function nextStepIcon(ratio) {
    sbn.setIconPref(ratio);
    document.getElementById("setup-iconsize").style.display="none";
    document.getElementById("setup-end").style.display="block";
    document.getElementById("setup-icon-ratio").src=sbnfs + sbnfs_setupPath + "/seubin.sbfs/meting/" + sbn.iconPref() + ".png";
    document.getElementById("setup-theme-pref").innerHTML=d.getElementById("setup-theme-pref").innerHTML + " " + sbn.themePref();
}

function nextStepEnd() {
    localStorage.setItem('seubinSetupEnded', 'true');
    alert(d.getElementById("nowRebootMsgSetup").innerHTML);
    sbn.reboot();
}

function startSetup() {
    sbn.setGender("anon");
    document.getElementById("rellodhimself").style.display="none"; //Hide normal rellod
    document.getElementById("booklist").style.display="none"; //Hide the apps
    document.getElementById("unrellodhimself").style.display="block"; //Show disabled rellod
    document.getElementById("setup").style.display="block"; //Show setup div
    document.getElementById("setup-tung").style.display="block"; //Show language section
    document.getElementById("setup-theme").style.display="none"; //Hide the other sections we don't want the user to see right now...
    document.getElementById("setup-iconsize").style.display="none";
    document.getElementById("setup-end").style.display="none";
}
