#!/usr/bin/env bash

if [[ "$1" = "" ]]; then
    mkdir ./build
    mkdir ./build/wono.shell.sbfs

    cp -a ./src/* ./build/wono.shell.sbfs
    cp -a ./seubin.meta/* ./build/wono.shell.sbfs

elif [[ "$1" = "wpkg" ]]; then

    [[ -d ./build ]] || ./build.sh

    WPKGNAME="wono.shell"
    WPKGVER=$(cat ./build/wono.shell.sbfs/book.json | jq -r '.version')

    mkdir ./wpkg-archive
    mkdir ./wpkg-archive/sbfs

    cp -a ./build/wono.shell.sbfs/* ./wpkg-archive/sbfs
    touch ./wpkg-archive/WHERE
    echo "setup" > ./wpkg-archive/WHERE
    touch ./wpkg-archive/SBFSNAME
    echo "$WPKGNAME" > ./wpkg-archive/SBFSNAME
    cd ./wpkg-archive
    zip -r ../${WPKGNAME}@${WPKGVER}.wpkg ./
    cd ..
    rm -rf ./wpkg-archive

elif [[ "$1" = "clean" ]]; then
    rm -rf ./build
fi
