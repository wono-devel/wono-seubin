if (seubinver.spec < "1.3") {
    document.querySelector("body").innerHTML = "<p>Book failed to start, check the JavaScript Console for more details</p> <br/> <p>BAD_SEUBIN_SPEC</p>";
    alert("Wrong Specification, Expecting 1.3");
    console.log("MISTAKE: Book is expecting 1.3, but a different version is reported");
    stopAllExecution();
}

document.body.style.backgroundImage = "url('" + sbnfs + sbnfs_setupPath + "/seubin.sbfs/meting/artwork_" + sbn_theme + ".png')"; 

async function getTungJSON() {  // Get and load tung JSON
    try {
        const tjson = await fetch("./tungs/" + sbn.tungPref() + ".json");
        return await tjson.json();
    } catch (error) {
        const tjson = await fetch("./tungs/en.json"); //Download English json if the language's json cannot be downloaded
        return await tjson.json();
    }
}

async function bookMain() { // Main function
    const tung = await getTungJSON();
        tungptc = JSON.stringify(tung).replace(/%%/g, seubinver.name).replace(/%&%/g, seubinver.ver); //Replace %% with strings
        tungwk = JSON.parse(tungptc);
    document.querySelector("title").innerHTML = tungwk.title;
    document.getElementById("wsbnver").innerHTML = tungwk.version;
    document.getElementById("meaning").innerHTML = tungwk.msg;
}

bookMain();

document.getElementById("theme").href = "css/" + sbn_theme + ".css";