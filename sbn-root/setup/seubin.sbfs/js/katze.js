/*libKatze v0.4*/
const d = document

function kprint(ctext) {
    console.log(ctext);
}

function kclear() {
    console.clear
}

const katze = {

    ver : function() {
        return 0.4
    },

//LocalStorage stuff

    mkLsKey : function(lskey, lskval) {
        localStorage.setItem(lskey, lskval);
        console.log("Wrote local storage key at /setup/settings.aso/local/" + lskey);
    },

    rdLsKey : function(lskey) {
        return localStorage.getItem(lskey);
    },

    rmLsKey : function(lskey) {
        localStorage.removeItem(lskey);
        console.log("Wiped local storage key at /setup/settings.aso/local/" + lskey);
    },

    rmLs : function() {
        localStorage.clear();
        console.log("Wiped /ware/ls0");
    },

//Cookies

    mkCky : function(ckyName, ckyVal) {
        document.cookie = ckyName + "=" + ckyVal;
    },

    mkCkyWD : function(ckyName, ckyVal, expire) {
        document.cookie = ckyName + "=" + ckyVal + "; expires=";
    },

    lsCkys : function() {
        allCookies = document.cookie;
        return allCookies;
    },

    rmCky : function(ckyName) {
        document.cookie = ckyName + "=deleted; expires=" + new Date().toUTCString();
    },

    rmCkys : function() {
        for (const cookie of document.cookie.split(";")) {
            const name = cookie.indexOf("=") > -1 ? cookie.substr(0, cookie.indexOf("=")) : cookie;
            document.cookie = name + "=;expires=01 Jan 1970";
        }
    },

//General Purpose

    shHrefWID : function(elemid, newhref) {
        document.getElement(elemid).href = newhref;
    },

    svStrAFl : function(tIF) {
        const txtblob = new Blob([tIF], {type: "text/plain"})
        txtbloburl = URL.createObjectURL(txtblob)
    },

    clearSSAFBlob : function() {
        URL.revokeObjectURL(txtbloburl)
    }

}

const k = katze
