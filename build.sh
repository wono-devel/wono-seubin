#!/usr/bin/env bash

if [[ $1 = "clean" ]]; then
    rm -rf ./build
    cd wono-seubin-shell
    ./build.sh clean
    cd ..
    rm -rf ./seubinknow
elif [[ $1 = "rom" ]]; then
    if [[ -d ./build/sbn-root ]]; then
        cd ./build
        zip -r ../wono-seubin.1.3.seubin ./
    else
        ./build.sh
        ./build.sh rom
    fi
else
    cd wono-seubin-shell
    ./build.sh
    cd ..
    [[ -d "seubinknow" ]] || git clone https://codeberg.org/meucit/seubinknow
    cd seubinknow
    ./build.sh
    cd ..
    mkdir ./build
    mkdir ./build/sbn-root
    cp -a ./sbn-root/* ./build/sbn-root
    cp -a ./wono-seubin-shell/build/wono.shell.sbfs ./build/sbn-root/setup
    mkdir ./build/sbn-root/things
    cp -a ./seubinknow/build/meucit.seubinknow.sbfs ./build/sbn-root/things
    rm -rf ./build/sbn-root/setup/seubin.sbfs/meting/*.xcf
    rm -rf ./build/sbn-root/setup/seubin.sbfs/meting/*.kra
    rm -rf ./build/sbn-root/setup/seubin.sbfs/meting/*.comment
    touch ./build/NEEDED_WRAP_VERSION
    echo "5" > ./build/NEEDED_WRAP_VERSION
fi
